void main() {

  var app = App(app_name: 'Med_It', sector: 'best app of the year', developer: 'tshepo', winning_year: '2022');
  
  app.printer();
  app.trasformName();
  
}

class App 
{
  //initialising our app properties
    String app_name  = '';
    String sector  = '';
    String developer  = '';
    String winning_year  = '';

      //creating a constructor
    App ({ required String this.app_name, required String this.sector, required String this.developer, required String this.winning_year});
    
    //prints our object which is app
    void printer()
      {
        print('The app name is: ${this.app_name}');
        print('The app sector is: ${this.sector}');
        print('The app develper is: ${this.developer}');
        print('Winning year for the app is: ${this.winning_year}');
      }

 
      void trasformName(){

        String trasformed_name;
         trasformed_name = this.app_name;

         trasformed_name = trasformed_name.toUpperCase();
         print('Capitalized app name: ${trasformed_name}');
      }
}